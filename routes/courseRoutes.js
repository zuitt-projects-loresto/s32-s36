const express = require('express');
const router = express.Router();

const courseControllers = require('../controllers/courseControllers');

const auth = require("../auth");

// to allow only Admins to add course
const {verify, verifyAdmin} = auth;


// Routes

// Create new course
router.post("/", verify, verifyAdmin, courseControllers.createCourse);

// Get all courses
router.get("/", courseControllers.getAllCourses);

// Get single course
router.get("/getSingleCourse/:id", courseControllers.getSingleCourseController);

// Archive a Course
router.put("/archive/:id", verify, verifyAdmin, courseControllers.archiveCourse);

// Activate a Course
router.put("/activate/:id", verify, verifyAdmin, courseControllers.activateCourse);

// Get All Active Courses
router.get("/getActiveCourses", courseControllers.getActiveCourses);

// Update a Course
router.put("/:id", verify, verifyAdmin, courseControllers.updateCourse);

// Get All Inactive Courses
router.get("/getInactiveCourses", verify, verifyAdmin, courseControllers.getActiveCourses);

// Find courses by name
router.post("/findCoursesByName", courseControllers.findCoursesByName);

// Find course by price
router.post("/findCoursesByPrice", courseControllers.findCoursesByPrice)

// Get Course Enrollees
router.get("/getEnrollees/:id", verify, verifyAdmin, courseControllers.getEnrollees);


module.exports = router;