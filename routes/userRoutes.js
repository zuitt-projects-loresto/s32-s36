const express = require('express');
const router = express.Router();

// import user controllers
const userControllers = require('../controllers/userControllers');

const auth = require('../auth');

const {verify, verifyAdmin} = auth;

// Routes

// User Registration
router.post("/", userControllers.registerUser);

// Retrieve all users
router.get("/", userControllers.getAllUsers);

// Login
router.post("/login", userControllers.loginUser);

// Retrieve User Details
router.get("/getUserDetails", verify, userControllers.getUserDetails);

// Check Email if Existing
router.post("/checkEmailExists", userControllers.checkEmailExists);

// Capture ID and update from regular to admin
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin);

// Update user details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

// Enroll registered user
router.post("/enroll", verify, userControllers.enroll);

// Get enrollments of the logged in user
router.get("/getEnrollments", verify, userControllers.getEnrollments);

module.exports = router;